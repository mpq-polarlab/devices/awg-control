#!/usr/bin/env python

import pyvisa
from time import sleep
import argparse
import struct
import numpy as np
import csv
import os
import time



num=0
tlast=-1

#get sample rate
NAME='arb'
SAMPLE_RATE=1e4



def load_waveform(inst, signal, sample_rate=1e4, nrep=5, datatype='f'):

    #sent start control message
    message="Uploading\nArbitrary\nWaveform"
    inst.write("DISP:TEXT '"+message+"'")

    #create a directory on device (will generate error if the directory exists, but we can ignore that)
    #inst.write("MMEMORY:MDIR \"INT:\\remoteAdded\"")

    #set byte order
    inst.write('FORM:BORD SWAP')
    
    
    for i in range(nrep):
        
        tstart = time.time()
    
        #clear volatile memory
        inst.write('SOUR1:DATA:VOL:CLE')

        #write arb to device
        ret=inst.write_binary_values('SOUR1:DATA:ARB '+NAME+',', signal, datatype=datatype, is_big_endian=False)
        
        
        tend=time.time()
        #wait until that command is done
        inst.write('*WAI')
        
        
        print(f"transfer rate = {ret*8/(1024*1024)/(tend-tstart):.2f} Mbits/s; total number of bytes = {ret}; total number of samples = {len(signal)}; total time = {tend-tstart} s")
        

    #NAME the arb
    inst.write('SOUR1:FUNC:ARB '+NAME)

    #set sample rate, voltage
    inst.write('SOUR1:FUNC:ARB:SRAT ' + str(sample_rate))
    inst.write('SOUR1:VOLT:OFFS 0')
    inst.write('SOUR1:FUNC ARB')
    inst.write('SOUR1:VOLT 1')

    #save arb to device internal memory
    #inst.write('MMEM:STOR:DATA "INT:\\remoteAdded\\'+NAME+'.arb"')

    #clear message
    inst.write("DISP:TEXT ''")
    
    return

    #check for error messages
    instrument_err = "error"
    while instrument_err != '+0,"No error"\n':
        inst.write('SYST:ERR?')
        instrument_err = inst.read()
        if instrument_err[:4] == "-257":  #directory exists message, don't display
            continue;
        if instrument_err[:2] == "+0":    #no error
            continue;
        print(instrument_err)